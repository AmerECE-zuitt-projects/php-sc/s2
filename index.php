<?php require_once "./code.php"?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S2: Activity</title>
</head>
<body>
    <!-- Loops -->
    <h2>Divisible of Five</h2>
    <?php divisibleByFive();?>

    <!-- Array Manipulation -->
    <h2>Array Manipulation</h2>
    <?php $students = [];?>
    <p><?php array_push($students, "Amer Qasem");?></p>
    <pre><?php print_r($students)?></pre>
    <p><?=count($students);?></p>

    <p><?php array_push($students, "Ahmed Kasem");?></p>
    <pre><?php print_r($students)?></pre>
    <p><?=count($students);?></p>

    <?php array_shift($students);?>
    <pre><?php print_r($students)?></pre>
    <p><?=count($students);?></p>
</body>
</html>